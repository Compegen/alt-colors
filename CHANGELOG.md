Changelog
====
All changes will be documented to this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/) and does not adhere to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

This project follows the pattern: `[RELEASE]-[VERSION][PATCH]`

## ALPHA
<details><summary>**A3**</summary>   

##### Added #####
- New Splashes
- New Main Menu
- Added recipes for metal chest upgrades
- Included more books in Wrapped Book
- Cleaned up some configs
- Added compressed cobble, dirt, sand, and gravel
----
- AE2 Stuff
- Aether II
- Aether Legacy
- Atlas Extras
- BASE
- YUNG'S Better Caves
- Better Nether
- Blood Arsenal
- Blood Magic
- Botania
- Brandon's Core
- Ceramics
- Cucumber
- Culinary Construct
- Custom Main Menu
- Draconic Evolution
- Forestry
- Immersive Hempcraft
- JEI Bees
- Magic Bees
- Mod Tweaker
- Modular Power Suits
- More Mystcraft
- MTLib
- Mystical Agriculture
- NuclearCraft
- Numina
- Pages
- Parry
- Pretty Beaches
- Randomthings
- Ranged Pumps
- Reliquary
- Save My Stronghold!
- Signpost
- Storage Drawers Unlimited
- Water Mechanics Backport
- Water Strainer

##### Changed ######
- Coral Reef density decreased
- Coral Reef light decreased
- Disabled Cyclops core analytics
- Disabled Defiled Hills biome (overpopulated BOP)
- Dynamic Trees yield increased
- Disabled Forge version check
- Reduced chance of Alutin holes in Futurepack
- Increased rate of Uranium in the world
- Disabled amethyst in Mystical World
- Added Defiled Stone to Ore Stone Variants (properly this time)
- Disabled PlusTiC Botania integration (using Pewter's)
- Quark frogs disabled, rock clusters made larger but rarer
- Enabled Minecraft Booster in SBRBF
- Updated launching splash
- Techguns: Disabled FOV speed lock, significantly reduced rate of mobs
- Increased amount of experience needed per level for Tinker Tool Leveling
- Added tip about Charm crates
- Enabled replaceErrorNotification in VanillaFix
- Disabled useless & overlap blocks in Future MC
- Updated JEI Blacklist
- Removed JER worldgen tab since its incompatible with Ore Stone Variants
- Reduced weight of Fox Hounds in Nether
- Adjusted Metal Chests to follow proper upgrade path
----
- Actually Additions
- Antique Atlas
- Apotheosis
- Chisel
- Colytra
- Construct's Armory
- Controlling
- CraftTweaker
- CreativeCore
- CTM
- Cuisine
- Cyclic
- Cyclops Core
- Defiled Lands
- Durability Viewer
- Dynamic Trees
- EnderIO
- EnderCore
- Erebus
- EvilCraft
- Extra Alchemy
- Fast Workbench
- Foamfix
- Forgelin
- FTBLib
- FTBUtils
- FTBUtilsBackups
- Future MC
- FuturePack
- Ido
- Immersive Engineering
- Inspirations
- JEHC
- Key Wizard
- LibraryEx
- LittleTiles
- Lost Cities
- Magic Bees
- Mario 2
- Mekanism
- Mystical World
- MysticalLib
- NetherEX
- OmLib
- Open Modular Turrets
- Ore Stone Variants
- PlusTiC
- Quark
- Random Patches
- Rats
- RayCore
- RFTools
- Roots
- Rustic
- Storage Drawers
- Tinker's Construct
- Tesla Core
- The Between Lands
- Tinker's Complement
- Unlimited Chiselworks
- Vanilla Fix
- Water Mechanics Backport

##### Removed #####
- Cleaned uped un-used configs & scripts

- Growthcraft (Not enough outside of Rustic & Pams)
- Immersive Railroading (significant impact to load times)
- [Disabled] Inventory Pets (no usage)
- [Disabled] Quark Oddities (no usage)
- Wards (dont work with modded books)
- [Disabled] Worley's Caves (replaced with Better Caves)

</details>
.


<details><summary>**A2**</summary>

##### Added #####
- Changelog
- No MarioMod biome
- No overlapping metals with MysticalWorld
- Metal chest sizes adjusted
- Added smaller LLoverlay texture
- Void generator texture
- Created food dictionary for cuisine/ nutrition
- Adjusted cost of colossal chests to better reflect other storage solutions
- Adjusted cost of metal chests to better reflect other storage solutions
- Adjusted cost of satchels to better reflect other storage solutions
- Adjusted cost of storage crates to better reflect other storage solutions

- - - -
- AIImprovements
- FiveFeetSmall
- Growthcraft
- Mario2
- MetalChests
- MinervaLibrary (new dependency for ExtraAlchemy)
- MysticalLib
- MysticalWorld
- NoRecipeBook
- Nutrition
- Peckish
- Roots
- TBone
- TheBetweenLands
- Unloader
- VanillaSatchels
- WeirdingGadget
- WrapUp

##### Changed #####
- **actuallyadditions** - Disabled book on first craft
- **charm** - Enabled charm lanterns, updated tallow durability values to reflect mod changes 
- **codechickenlib** - Disabled logging RenderExceptions
- **colossalchests** - Disabled analytics, decreased max chest size
- **conarm** - Increase needed XP to level tools
- **cuisine** - Hardcore mode re-enabled, bamboo re-enabled
- **cyclicmagic** - Nether & End ores re-distributed
- **durabilityviewer** - Made tooltip less intrusive, disabled low durability sound
- **dynamictrees** - Disabled falling tree damage
- **erebus** - Players now respawn in dimension
- **ftbbackups** - Lowered total amount of backups & increased compression level
- **ftbutilities** - Enabled spawn command, disabled chunkloading, enabled safe spawn
- **LLOverlayReloaded** - Lowered active range from 3 to 2 chunks
- **MorePlayerModels** - Disabled tooltips, enabled compatibility mode
- **netherchest** - Removed particles
- **plustic** - Disabled diamantine & palis
- **quark** - 
    - Moved trash button above inventory
    - Disabled matrix enchanting
    - Sign editing requires empty hand
    - Increased spawns of Foxhounds
    - Increased rarity of magma biome
    - Decreased rarity of dungeons
- **randompatches** - Disabled force to titlescreen on disconnect
- **rustic** - Changed slate to spawn in nether; increased max slate vein size
- **splash.properties** - Changed colors :)
- **tcomplement** - Disabled bucket cast, decreased return of high oven, disabled stone blacklist for melter
- **tconstruct** - Increased rarity of slime islands
- **techguns** - Decreased weight of nether mobs
- **thaumcraft** - Greatly decreased taint spread
- **TinkerToolLeveling** - Capped tool leveling
- **tips** - Added new tips
- **totemic** - Removed night vision overlap with CoFH; disabled skeletons attacking buffalo
- **worleycaves** - Blacklisted LostCities, reduced cave noise, reduced surface holes, reduced warp
- **apotheosis/enchantments** - Increased max level of Hell Infusion, decreased holding level of CoFH
- **defaultoptions** - Updated
- **endercore/endercore** - Removed durability overlap, removed debug info overlap, changed how much water in water bottles
- **enderio/enderio** - Removed vanilla beheading overlap (apotheosis), disabled painter tooltip
- **futuremc** - Disabled lantern overlap (rustic)
- **inventorypets** - Disabled update handler
- **jei/itemBlacklist** - Updated
- **lostcities/general** - Added 2 new profiles, made default profile very rare BOP cities
- **randomtweaks/randomtweaks** - Disabled auto-thirdperson, disabled void world type
- - - - 
- **jei.zs** - Added description for how to obtain buffalo hide
- **recipes.zs** - Updated formatting
- **sbrbf-stuff.zs** - Added crafting to void generator, clarified Lost Cities portal instructions, changed miner's dream to use BOP gems
- - - -
- ActuallyAdditions
- Apotheosis
- BiomesOPlenty
- Charm
- CodeChickenLib
- Cyclic
- EnderCore
- EnderIO
- ExtraAlchemy
- FTBLib
- FTBUtilities
- ImmersiveEngineering
- InventoryPets
- JEHC (disabled)
- NetherEx
- Pam's Harvestcraft (disabled)
- Pam's Cookables (disabled)
- PlusTiC
- RandomPatches
- RedstonePCB
- StorageDrawers

##### Removed #####
- Cleaned upped un-used/data configs
- - - -
- Clarity (duplicated in RandomTweaks)
- FastLeafDecay (Replaced with TBone)
- ImprovedBackpacks (Replaced with Vanilla+ Satchels)
- Obfuscate (unused core mod)


</details>
.