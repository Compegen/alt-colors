import crafttweaker.item.IItemStack;
import mods.jei.JEI;

hideRegex("ore_stone_variants.*");


JEI.addItem(<fp:spaceship:39>);

//JEI.addDescription(<rats:chunky_cheese_token>, "Brings you to a sunken land of old");
JEI.addDescription(<totemic:buffalo_items:0>, "Obtained by performing the Buffalo Dance near cows");


////////////////////////////////////////////////////////////////////////////////////////////
function removeRegex(exp as string) {
	var allItems = itemUtils.getItemsByRegexRegistryName(exp) as IItemStack[];
	for item in allItems { JEI.removeAndHide(item); }
}
function hideRegex(exp as string) {
	var allItems = itemUtils.getItemsByRegexRegistryName(exp) as IItemStack[];
	for item in allItems { JEI.hide(item); }
}
////////////////////////////////////////////////////////////////////////////////////////////