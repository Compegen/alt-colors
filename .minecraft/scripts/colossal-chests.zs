recipes.removeByRegex("colossalchests:chest_wall.*");
recipes.remove(<colossalchests:uncolossal_chest>);

var i1 = <actuallyadditions:block_misc:4>; var i2 = <ore:plankWood>;

recipes.addShaped("AC_CW_W", <colossalchests:chest_wall:0>*2, [[i2, i2, i2], [i2, i1, i2], [i2, i2, i2]]);

i2 = <ore:ingotCopper>; recipes.addShaped("AC_CW_C", <colossalchests:chest_wall:1>*2,[[i2,i2,i2],[i2,i1,i2],[i2,i2,i2]]);
i2 = <ore:ingotIron>; recipes.addShaped("AC_CW_I", <colossalchests:chest_wall:2>*2,[[i2,i2,i2],[i2,i1,i2],[i2,i2,i2]]);
i2 = <ore:ingotSilver>; recipes.addShaped("AC_CW_S", <colossalchests:chest_wall:3>*2,[[i2,i2,i2],[i2,i1,i2],[i2,i2,i2]]);
i2 = <ore:ingotGold>; recipes.addShaped("AC_CW_G", <colossalchests:chest_wall:4>*2,[[i2,i2,i2],[i2,i1,i2],[i2,i2,i2]]);
i2 = <ore:gemDiamond>; recipes.addShaped("AC_CW_D", <colossalchests:chest_wall:5>*2,[[i2,i2,i2],[i2,i1,i2],[i2,i2,i2]]);
i2 = <ore:obsidian>; 
recipes.addShaped("AC_CW_O", <colossalchests:chest_wall:6>*2,[[null,i2,null],[i2,<colossalchests:chest_wall:5>,i2],[null,i2,null]]);


i1 = <ore:slabWood>;
recipes.addShaped("AC_UCC", <colossalchests:uncolossal_chest>, [[i1, i1, i1], [i1, null, i1], [i1, i1, i1]]);