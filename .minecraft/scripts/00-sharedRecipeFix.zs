val n = null;

//Smart Wrench & LittleChisel
val lap = <ore:gemLapis>; val ii = <ore:ingotIron>;
recipes.removeByRecipeName("littletiles:chisel_0"); recipes.removeByRecipeName("rftools:smartwrench");
recipes.addShaped("SBRBF_chisel", <littletiles:chisel:0>, [[ii,n,n],[n,lap,n],[n,n,lap]]);
recipes.addShaped("SBRBF_sw", <rftools:smartwrench>, [[n,n,ii],[n,lap,n],[lap,n,n]]);

/*
// MKN Utils smooth stone
recipes.removeByRecipeName("mkutils:smoothstone");
*/