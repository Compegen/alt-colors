// Big Chests

recipes.remove(<actuallyadditions:block_giant_chest>);
recipes.remove(<actuallyadditions:block_giant_chest_medium>);
recipes.remove(<actuallyadditions:block_giant_chest_large>);

var i1 = <ore:chestWood>; var i2 = <actuallyadditions:block_misc:4>;
recipes.addShaped("AC_storagecrate", <actuallyadditions:block_giant_chest>, [[i1, i2, i1], [i2, <actuallyadditions:block_misc:9>, i2], [i1, i2, i1]]);

i1 = <actuallyadditions:block_crystal:3>; i2 = <actuallyadditions:block_misc:4>;
recipes.addShaped("AC_storagecrateM", <actuallyadditions:block_giant_chest_medium>, [[i1, i2, i1], [i2, <actuallyadditions:block_giant_chest>, i2], [i1, i2, i1]]);

i1 = <actuallyadditions:block_crystal_empowered:3>; i2 = <actuallyadditions:block_misc:4>;
recipes.addShaped("AC_storagecrateL", <actuallyadditions:block_giant_chest_large>, [[i1, i2, i1], [i2, <actuallyadditions:block_giant_chest_medium>, i2], [i1, i2, i1]]);