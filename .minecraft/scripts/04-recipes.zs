val n = null;
var i1 = <ore:leather>; var i2 = <ore:enderpearl>; var i3 = <ore:stickTreatedWood>;

// Cyclic //////////////////////////////////////////////////
recipes.remove(<cyclicmagic:chest_sack_empty>);
recipes.remove(<cyclicmagic:storage_bag>);
recipes.addShaped("AC_Sack", <cyclicmagic:chest_sack_empty>, [[n, <ore:string>, n], [i1, i2, i1], [i1, i1, i1]]);
i1 = <ore:hideBuffalo>;
recipes.addShaped("AC_Bag", <cyclicmagic:storage_bag>, [[n, <ore:string>, n], [i1, <ore:alloyBasic>, i1], [i1, i1, i1]]);
recipes.addShapeless("AC_CyclicBook", <guideapi:cyclicmagic-guide>, [<ore:stickWood>, <ore:book>]);


//Embers //////////////////////////////////////////////////
recipes.removeByRegex("embers:ingot(?!dawnstone).*");
recipes.remove(<embers:grandhammer>);
recipes.remove(<embers:pickaxe_clockwork>);
recipes.remove(<embers:axe_clockwork>);

i1 = <ore:ingotDawnstone>; i2 = <ore:blockDawnstone>; i3 = <ore:stickTreatedWood>;
recipes.addShaped("AC_Grandhammer", <embers:grandhammer>, [[i2, i1, i2], [n, i3, n], [n, i3, n]]);
i2 = <embers:shard_ember>;
recipes.addShaped("AC_Clockwork", <embers:pickaxe_clockwork>, [[i1, i2, i1], [n, i3, n], [n, i3, n]]);
i1 = <ore:plateDawnstone>;
recipes.addShaped("AC_Battleaxe", <embers:axe_clockwork>, [[i1, i2, i1], [i1, i3, i1], [n, i3, n]]);


//EvilCraft	//////////////////////////////////////////////////
recipes.remove(<evilcraft:dark_stick>);
recipes.addShaped("AC_DS", <evilcraft:dark_stick>, [[<defiledlands:vilespine>],[<defiledlands:vilespine>]]);


// Improved Backpacks //////////////////////////////////////////////////
/*
recipes.remove(<improvedbackpacks:bound_leather>);
recipes.replaceAllOccurences(<improvedbackpacks:tanned_leather>, <ore:leather>);
*/


// Futurepack //////////////////////////////////////////////////
recipes.addShapeless("AC_MysteriousDocument", <fp:spaceship:39>, [<ore:paper>, <fp:neon_sand:*>]);


// EnderIO
recipes.replaceAllOccurences(<enderio:item_material:48>, <ore:dyeGreen>);
recipes.replaceAllOccurences(<enderio:item_material:49>, <ore:dyeBrown>);
recipes.replaceAllOccurences(<enderio:item_material:50>, <ore:dyeBlack>);

// Mystical Agriculture
i1 = <ore:stone>; i2 = <ore:soulSand>;
recipes.removeByRecipeName("mysticalagriculture:soulstone");
recipes.addShaped("AC_soulstone", <mysticalagriculture:soulstone:0>, [[i1, i2],[i2, i1]]);