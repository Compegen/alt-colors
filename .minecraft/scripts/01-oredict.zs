import crafttweaker.oredict.IOreDict;
import crafttweaker.oredict.IOreDictEntry;


// Crystal Clusters
val cc = <ore:crystalCluster>;
cc.add(<actuallyadditions:block_crystal_cluster_redstone>);
cc.add(<actuallyadditions:block_crystal_cluster_lapis>);
cc.add(<actuallyadditions:block_crystal_cluster_diamond>);
cc.add(<actuallyadditions:block_crystal_cluster_coal>);
cc.add(<actuallyadditions:block_crystal_cluster_emerald>);
cc.add(<actuallyadditions:block_crystal_cluster_iron>);

// Futurepack Stones
<ore:stone>.remove(<fp:stone:8>);
<ore:cobblestone>.remove(<fp:stone:7>);

// Mystical Agriculture
//<ore:soulsand>.add(<mysticalagriculture:soulstone>);


// Food dictionary
val rice = <ore:foodRice>;
val grain = <ore:listAllgrain>;


//rice.remove(<growthcraft_rice:rice_ball>);
rice.add(<actuallyadditions:item_food:16>);

val rawmeat = <ore:listAllmeatraw>;
val cookmeat = <ore:listAllmeatcooked>;
val fish = <ore:fish>;