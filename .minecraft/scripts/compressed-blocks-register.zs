#loader contenttweaker
#debug

import mods.contenttweaker.VanillaFactory;
import mods.contenttweaker.Block;


val tiers = 8;
	
for i in 0 .. tiers {
	var cobble = VanillaFactory.createBlock("c_cobble"+i, <blockmaterial:rock>);
	var dirt = VanillaFactory.createBlock("c_dirt"+i, <blockmaterial:ground>);
	var sand = VanillaFactory.createBlock("c_sand"+i, <blockmaterial:sand>);
	var gravel = VanillaFactory.createBlock("c_gravel"+i, <blockmaterial:ground>);
	
	cobble.setBlockHardness(2.0*(1+i));
	cobble.setBlockResistance(6.0*(1+i));
	cobble.setToolClass("pickaxe");
	cobble.setToolLevel(0);
	cobble.setBlockSoundType(<soundtype:stone>);
	cobble.register();
	
	dirt.setBlockHardness(0.5*(1+i));
	dirt.setBlockResistance(0.5*(1+i));
	dirt.setToolClass("shovel");
	dirt.setToolLevel(0);
	dirt.setBlockSoundType(<soundtype:ground>);
	dirt.register();
	
	sand.setBlockHardness(0.5*(1+i));
	sand.setBlockResistance(0.5*(1+i));
	sand.setToolClass("shovel");
	sand.setToolLevel(0);
	sand.setBlockSoundType(<soundtype:sand>);
	sand.register();
	
	gravel.setBlockHardness(0.6*(1+i));
	gravel.setBlockResistance(0.6*(1+i));
	gravel.setToolClass("shovel");
	gravel.setToolLevel(0);
	gravel.setBlockSoundType(<soundtype:ground>);
	gravel.register();
}