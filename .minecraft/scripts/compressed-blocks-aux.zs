import crafttweaker.item.IItemStack;

var blocks = [<minecraft:cobblestone>, <minecraft:dirt>, <minecraft:sand>, <minecraft:gravel>] as IItemStack[];
var names = ["cobble", "dirt", "sand", "gravel"] as string[];

val tiers = 8;

var x = <minecraft:cobblestone>;

for p, n in names {
	
	var b = blocks[p];
	for i in 0 .. tiers {
		var amt = pow(9.0, i+1);
		
		x = itemUtils.getItem("contenttweaker:c_"~n~i) as IItemStack;
		recipes.addShapeless("AC_c_"~n~i, x, [b,b,b, b,b,b, b,b,b]);
		recipes.addShapeless("AC_uc_"~n~i, b*9, [x]);
		var locale = "Compressed "~blocks[p].displayName~" x"~(i+1) as string;
		x.displayName = locale;
		x.addTooltip(amt~" "~n);
		b = x;
	}
}