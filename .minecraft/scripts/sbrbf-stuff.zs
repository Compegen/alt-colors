import crafttweaker.item.IItemStack;
import crafttweaker.item.IIngredient;
import crafttweaker.block.IBlock;
import crafttweaker.block.IBlockDefinition;
import crafttweaker.oredict.IOreDictEntry;


// Minecraft

var i1 = <ore:blockGlass>; var i2 = <ore:obsidian>;

recipes.removeByRecipeName("minecraft:beacon");
recipes.addShaped("SBRBF_beacon", <minecraft:beacon>, [[i1, i1, i1], [i1, <ore:blockDiamond>, i1], [i2, i2, i2]]);

/*
recipes.addShapeless("AC_tM2S", <sbrbf:enchantmenttable>, [<minecraft:enchanting_table>]);
recipes.addShapeless("AC_tS2M", <minecraft:enchanting_table>, [<sbrbf:enchantmenttable>]);
*/

////////////////////////////////////////////////////////////////////////////////////////////
// VOID GENERATOR
////////////////////////////////////////////////////////////////////////////////////////////
val vg = <sbrbf:voidgenerator>;
val vg_block = vg.asBlock().definition;

vg_block.setHarvestLevel("pickaxe", 0);
vg_block.hardness = 3.5;

i1=<mekanism:basicblock:10>; i2=<thermalfoundation:material:514>;
recipes.addShaped("AC_VG", <sbrbf:voidgenerator>, [[i1, <ore:pearlFluix>, i1], [i1, <thermalexpansion:frame:0>, i1], [i1, i2, i1]]);


////////////////////////////////////////////////////////////////////////////////////////////
// PORTAL BLOCK
////////////////////////////////////////////////////////////////////////////////////////////
val portalblock = <sbrbf:block1>;
val pb_block = portalblock.asBlock().definition;

portalblock.displayName = "Crimson Obsidian";
mods.jei.JEI.addDescription(portalblock, "Used to make City Portals. Activate with gold nugget");

pb_block.setHarvestLevel("pickaxe", 3);
portalblock.hardness = 50.0;

i1 = <biomesoplenty:fleshchunk>;
recipes.addShaped("SBRBF_Portal", portalblock, [[null, i1, null], [i1, i2, i1], [null, i1, null]]);



////////////////////////////////////////////////////////////////////////////////////////////
// YOGURT
////////////////////////////////////////////////////////////////////////////////////////////
val yogurt = <sbrbf:block3>;
val y_block = yogurt.asBlock().definition;

yogurt.displayName = "Yogurt";
mods.jei.JEI.addDescription(yogurt, "Only the ancient rats of old knew how to make such a delicious dairy product");

y_block.setHarvestLevel("shovel", 0);

i1 = <sbrbf:block3>;
recipes.addShaped("AC_SM", <sbrbf:itemspiceman>, [[null, i1, null], [i1, <cyclicmagic:soulstone>, i1], [null, i1, null]]);


////////////////////////////////////////////////////////////////////////////////////////////
// Reactive Flint
////////////////////////////////////////////////////////////////////////////////////////////
val rflint = <sbrbf:item4>;
rflint.displayName = "Reactive Flint";
recipes.addShapeless("AC_ReFl", rflint, [<ore:itemFlint>, <ore:dustRedstone>]);


////////////////////////////////////////////////////////////////////////////////////////////
// WRAPPED STARTER BOOK
////////////////////////////////////////////////////////////////////////////////////////////
//val stbk = <sbrbf:item3>;
//stbk.displayName = "Wrapped Book";
//stbk.addTooltip("Craft to get the starting books");

//recipes.addShapeless("AC_SB", <akashictome:tome>.withTag({"akashictome:data": {tconstruct: {id: "tconstruct:book", Count: 1 as byte, tag: {"akashictome:definedMod": "tconstruct"}, Damage: 0 as short}, conarm: {id: "conarm:book", Count: 1 as byte, tag: {"akashictome:definedMod": "conarm"}, Damage: 0 as short}, cuisine: {id: "cuisine:manual", Count: 1 as byte, tag: {"akashictome:definedMod": "cuisine"}, Damage: 0 as short}, fp: {id: "fp:spaceship", Count: 1 as byte, tag: {"akashictome:definedMod": "fp"}, Damage: 39 as short}}}), [<sbrbf:item3>]);


////////////////////////////////////////////////////////////////////////////////////////////
// MINERS DELIGHT
////////////////////////////////////////////////////////////////////////////////////////////
val minedel = <sbrbf:itemminersdream>;
val gems = <ore:gemAmethyst> | <ore:gemPeridot> | <ore:gemTopaz> | <ore:gemTanzanite> | <ore:gemMalachite> | <ore:gemSapphire>; 
recipes.remove(minedel);
val rb=<ore:blockRedstone>; val efln = <tconstruct:throwball:1>;
recipes.addShaped("SBRBF_minerdream", minedel, [[gems, gems, gems],	[rb, rb, rb], [efln, efln, efln]]);
