// Lexica Botania
val magPlant= <botania:flower:*> | <botania:mushroom:*>;
recipes.remove(<botania:lexicon>);
recipes.addShapeless("AC_Lexica", <botania:lexicon>, [<ore:book>, magPlant]);