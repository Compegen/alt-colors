import crafttweaker.item.IItemStack;
import crafttweaker.oredict.IOreDictEntry;


recipes.removeByRegex("metalchests:.*");

// Copper, Iron, Silver, Gold, Diamond, Obsidian
<ore:chestHungry>.add(<thaumcraft:hungry_chest>);
val wc ={w:<ore:chestWood>, h:<ore:chestHungry>} as IOreDictEntry[string];
val ut = <storagedrawers:upgrade_template>;

val ing ={c:<ore:ingotCopper>, i:<ore:ingotIron>, s:<ore:ingotSilver>, g:<ore:ingotGold>, d:<ore:gemDiamond>, o:<ore:obsidian>} as IOreDictEntry[string];
val nug ={c:<ore:nuggetCopper>, i:<ore:nuggetIron>, s:<ore:nuggetSilver>, g:<ore:nuggetGold>, d:<ore:nuggetDiamond>, o:<ore:obsidian>} as IOreDictEntry[string];
//val mc = [<metalchests:metal_chest:0>, <metalchests:metal_chest:1>, <metalchests:metal_chest:2>, <metalchests:metal_chest:3>, <metalchests:metal_chest:4>, <metalchests:metal_chest:5>] as IItemStack[];
//val hc = [<metalchests:metal_hungry_chest:0>, <metalchests:metal_hungry_chest:1>, <metalchests:metal_hungry_chest:2>, <metalchests:metal_hungry_chest:3>, <metalchests:metal_hungry_chest:4>, <metalchests:metal_hungry_chest:5>] as IItemStack[];


var mc = [] as IItemStack[];
var hc = [] as IItemStack[];
for i in 0 to 6 {
	mc += <metalchests:metal_chest>.definition.makeStack(i);
	hc += <metalchests:metal_hungry_chest>.definition.makeStack(i);
	//mc += itemUtils.getItem("metalchests:metal_chest:"~i);
	//hc += itemUtils.getItem("metalchests:metal_chest:"~i);
}


var mcu = [] as IItemStack[];
var hcu = [] as IItemStack[];
for i in 0 to 21 {
	mcu += <metalchests:chest_upgrade>.definition.makeStack(i);
	hcu += <metalchests:hungry_chest_upgrade>.definition.makeStack(i);
}



// CHESTS //
//// Copper
recipes.addShaped("AC_MC_C", mc[0], [[nug.c, nug.c, nug.c], [nug.c, wc.w, nug.c], [nug.c, nug.c, nug.c]]);
recipes.addShaped("AC_HC_C", hc[0], [[nug.c, nug.c, nug.c], [nug.c, wc.h, nug.c], [nug.c, nug.c, nug.c]]);

//// Iron
recipes.addShaped("AC_MC_I", mc[1], [[ing.i, ing.i, ing.i], [ing.i, wc.w, ing.i], [ing.i, ing.i, ing.i]]);
recipes.addShaped("AC_HC_I", hc[1], [[ing.i, nug.i, ing.i], [ing.i, wc.h, nug.i], [ing.i, nug.i, ing.i]]);

recipes.addShaped("AC_MC_I2", mc[1], [[null, ing.i, null], [ing.i, mc[0], ing.i], [null, ing.i, null]]);
recipes.addShaped("AC_HC_I2", hc[1], [[null, ing.i, null], [ing.i, hc[0], ing.i], [null, ing.i, null]]);

//// Silver
recipes.addShaped("AC_MC_S2", mc[2], [[ing.s, nug.s, ing.s], [nug.s, mc[1], nug.s], [ing.s, nug.s, ing.s]]);
recipes.addShaped("AC_HC_S2", hc[2], [[ing.s, nug.s, ing.s], [nug.s, hc[1], nug.s], [ing.s, nug.s, ing.s]]);

//recipes.addShaped("AC_MC_S", mc[2], [[ing.s, ing.s, ing.s], [ing.s, mc[0], ing.s], [ing.s, ing.s, ing.s]]);
//recipes.addShaped("AC_HC_S", hc[2], [[ing.s, ing.s, ing.s], [ing.s, hc[0], ing.s], [ing.s, ing.s, ing.s]]);

//// Gold
recipes.addShaped("AC_MC_G2", mc[3], [[ing.g, nug.g, ing.g], [nug.g, mc[2], nug.g], [ing.g, nug.g, ing.g]]);
recipes.addShaped("AC_HC_G2", hc[3], [[ing.g, nug.g, ing.g], [nug.g, hc[2], nug.g], [ing.g, nug.g, ing.g]]);

//recipes.addShaped("AC_MC_G", mc[3], [[ing.g, ing.g, ing.g], [ing.g, mc[1], ing.g], [ing.g, ing.g, ing.g]]);
//recipes.addShaped("AC_HC_G", hc[3], [[ing.g, ing.g, ing.g], [ing.g, hc[1], ing.g], [ing.g, ing.g, ing.g]]);

//// Diamond
recipes.addShaped("AC_MC_D", mc[4], [[ing.d, ing.d, ing.d], [null, mc[3], null], [ing.d, ing.d, ing.d]]);
recipes.addShaped("AC_HC_D", hc[4], [[ing.d, ing.d, ing.d], [null, hc[3], null], [ing.d, ing.d, ing.d]]);

//// Obsidian
recipes.addShaped("AC_MC_O", mc[5], [[null, ing.o, null], [ing.o, mc[4], ing.o], [null, ing.o, null]]);
recipes.addShaped("AC_HC_O", hc[5], [[null, ing.o, null], [ing.o, hc[4], ing.o], [null, ing.o, null]]);


// UPGRADES //
recipes.addShaped("AC_mcu_0", mcu[0], [[nug.c, nug.c, nug.c], [nug.c, ut, nug.c], [nug.c, nug.c, nug.c]]);
recipes.addShaped("AC_mcu_1", mcu[1], [[ing.i, ing.i, ing.i], [ing.i, ut, ing.i], [ing.i, ing.i, ing.i]]);
recipes.addShaped("AC_mcu_6", mcu[6], [[null, ing.i, null], [ing.i, ut, ing.i], [null, ing.i, null]]);
recipes.addShaped("AC_mcu_11", mcu[11], [[ing.s, nug.s, ing.s], [nug.s, ut, nug.s], [ing.s, nug.s, ing.s]]);
recipes.addShaped("AC_mcu_15", mcu[15], [[ing.g, nug.g, ing.g], [nug.g, ut, nug.g], [ing.g, nug.g, ing.g]]);
recipes.addShaped("AC_mcu_18", mcu[18], [[ing.g, nug.g, ing.g], [nug.g, ut, nug.g], [ing.g, nug.g, ing.g]]);
recipes.addShaped("AC_mcu_20", mcu[20], [[ing.d, ing.d, ing.d], [null, ut, null], [ing.d, ing.d, ing.d]]);

recipes.addShapeless("AC_mcu2_0", mcu[0], [hcu[0]]);
recipes.addShapeless("AC_mcu2_1", mcu[1], [hcu[1]]);
recipes.addShapeless("AC_mcu2_6", mcu[6], [hcu[6]]);
recipes.addShapeless("AC_mcu2_11", mcu[11], [hcu[11]]);
recipes.addShapeless("AC_mcu2_15", mcu[15], [hcu[15]]);
recipes.addShapeless("AC_mcu2_18", mcu[18], [hcu[18]]);
recipes.addShapeless("AC_mcu2_20", mcu[20], [hcu[20]]);

recipes.addShapeless("AC_hcu_0", hcu[0], [mcu[0]]);
recipes.addShapeless("AC_hcu_1", hcu[1], [mcu[1]]);
recipes.addShapeless("AC_hcu_6", hcu[6], [mcu[6]]);
recipes.addShapeless("AC_hcu_11", hcu[11], [mcu[11]]);
recipes.addShapeless("AC_hcu_15", hcu[15], [mcu[15]]);
recipes.addShapeless("AC_hcu_18", hcu[18], [mcu[18]]);
recipes.addShapeless("AC_hcu_20", hcu[20], [mcu[20]]);