import crafttweaker.item.IItemStack;

////////////////////////////////////////////////////////////////////////////////////////////
// AE2 BLOCK
////////////////////////////////////////////////////////////////////////////////////////////
val chss = <sbrbf:block2>;
val chss_block = chss.asBlock().definition;
chss.displayName = "Etched Sky Stone";
chss_block.setHarvestLevel("pickaxe", 3);
chss.hardness = 10;

val cq = <appliedenergistics2:material:1>;
val cs = <sbrbf:block2>;

val presses = [ //Calculation, Engineering, Logic, Silicon
	<appliedenergistics2:material:13>,
	<appliedenergistics2:material:14>,
	<appliedenergistics2:material:15>,
	<appliedenergistics2:material:19>
] as IItemStack[];

recipes.addShaped("SBRBF_calc", presses[0], [[cs, cq, cs], [cq, cq, cs], [cs, cs, <appliedenergistics2:material:10>]]);
recipes.addShaped("SBRBF_engi", presses[1], [[cs, cq, cs], [cq, cq, cs], [cs, <ore:gemDiamond>, cs]]);
recipes.addShaped("SBRBF_logi", presses[2], [[cs, cs, cs], [cq, cq, cq], [cs, <ore:ingtGold>, cs]]);
recipes.addShaped("SBRBF_sili", presses[3], [[cs, cq, cs], [cq, <appliedenergistics2:material:5>, cq], [cs, cq, cs]]);