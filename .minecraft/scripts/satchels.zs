// CoFH Satchels //////////////////////////////////////////////////
recipes.removeByRecipeName("thermalexpansion:satchel");
recipes.removeByRecipeName("vanillasatchels:satchel");
recipes.removeByRegex("vanillasatchels:satchel_[1-4,6-9](0)*");
recipes.removeByRegex("thermalexpansion:satchel_[1,6,7]");

recipes.addShaped("AS_basic", <thermalexpansion:satchel:0>, [[<ore:ingotTin>, <vanillasatchels:satchel:0>, <ore:ingotTin>]]);

// Vanilla satchels
var i1 = <ore:leather>; var i2 = <ore:string>; var i3 = <ore:blockWool>;
recipes.addShaped("AS_satchel", <vanillasatchels:satchel:0>, [[i1,i2,i1],[i2,i3,i2],[i1,i2,i1]]);

i1 = <ore:ingotIron>; i2 = <ore:nuggetIron>; i3 = <vanillasatchels:satchel:0>;
recipes.addShaped("AS_satchel1", <vanillasatchels:satchel:1>, [[i1,i2,i1],[i2,i3,i2],[i1,i2,i1]]);

i1 = <ore:ingotGold>; i2 = <ore:nuggetGold>; i3 = <vanillasatchels:satchel:1>;
recipes.addShaped("AS_satchel2", <vanillasatchels:satchel:2>, [[i1,i2,i1],[i2,i3,i2],[i1,i2,i1]]);

i1 = <ore:gemDiamond>; i2 = <ore:nuggetDiamond>; i3 = <vanillasatchels:satchel:2>;
recipes.addShaped("AS_satchel3", <vanillasatchels:satchel:3>, [[i1,i2,i1],[i2,i3,i2],[i1,i2,i1]]);

i1 = <ore:gemEmerald>; i2 = <ore:nuggetEmerald>; i3 = <vanillasatchels:satchel:3>;
recipes.addShaped("AS_satchel4", <vanillasatchels:satchel:4>, [[i1,i2,i1],[i2,i3,i2],[i1,i2,i1]]);