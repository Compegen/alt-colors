import crafttweaker.item.IItemStack;

// Tinkers Compliment //////////////////////////////////////////////////
recipes.removeByRegex("tcomplement:armor/steel.*");
recipes.addShaped("AC_itemrack", <tconstruct:rack:0>*3, [[<tconstruct:rack:1>, <tconstruct:rack:1>]]);


// Tinkers //////////////////////////////////////////////////

val l = <tconstruct:bow_limb>.withTag({Material: "slime"}) | <tconstruct:bow_limb>.withTag({Material: "blueslime"}) | <tconstruct:bow_limb>.withTag({Material: "magmaslime"});
var rf = <sbrbf:item4>;

recipes.remove(<tconstruct:slimesling:*>);
recipes.addShaped("AC_S_0", <tconstruct:slimesling:0>, [[l, <ore:string>, <ore:string>], [null, <minecraft:slime_ball>, l], [l, null, null]]);
recipes.addShaped("AC_S_1", <tconstruct:slimesling:1>, [[l, <ore:string>, <ore:string>], [null, <tconstruct:edible:1>, l], [l, null, null]]);
recipes.addShaped("AC_S_2", <tconstruct:slimesling:2>, [[l, <ore:string>, <ore:string>], [null, <tconstruct:edible:2>, l], [l, null, null]]);
recipes.addShaped("AC_S_3", <tconstruct:slimesling:3>, [[l, <ore:string>, <ore:string>], [null, <tconstruct:edible:3>, l], [l, null, null]]);
recipes.addShaped("AC_S_4", <tconstruct:slimesling:4>, [[l, <ore:string>, <ore:string>], [null, <tconstruct:edible:4>, l], [l, null, null]]);
	
recipes.remove(<tconstruct:throwball:1>);
recipes.addShaped("AC_EFLN", <tconstruct:throwball:1>, [[null, rf, null], [rf, <thermalfoundation:material:1024>, rf], [null, rf, null]]);