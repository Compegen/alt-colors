import crafttweaker.item.IIngredient;
import crafttweaker.item.IItemStack;

////////////////////////////////////////////////////////////////////////////////////////////
// WRAPPED STARTER BOOK
////////////////////////////////////////////////////////////////////////////////////////////
val stbk = <sbrbf:item3>;
stbk.displayName = "Wrapped Book";
stbk.addTooltip("Craft to get the starting books");

recipes.addShapeless("AC_SB", <akashictome:tome>.withTag({"akashictome:data": {tconstruct: {id: "tconstruct:book", Count: 1 as byte, tag: {"akashictome:definedMod": "tconstruct"}, Damage: 0 as short}, conarm: {id: "conarm:book", Count: 1 as byte, tag: {"akashictome:definedMod": "conarm"}, Damage: 0 as short}, cuisine: {id: "cuisine:manual", Count: 1 as byte, tag: {"akashictome:definedMod": "cuisine"}, Damage: 0 as short}, fp: {id: "fp:spaceship", Count: 1 as byte, tag: {"akashictome:definedMod": "fp"}, Damage: 39 as short}, techguns: {id: "patchouli:guide_book", Count: 1 as byte, tag: {"akashictome:definedMod": "techguns", "patchouli:book": "techguns:techguns_manual"}, Damage: 0 as short}}}), [<sbrbf:item3>]);