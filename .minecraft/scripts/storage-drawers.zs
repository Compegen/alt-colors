// Storage Drawers
var i1 = <ore:stickWood>; var i2 = <tconstruct:pattern>;
recipes.removeByRecipeName("storagedrawers:upgrade_template");
recipes.addShaped("AC_uptemp", <storagedrawers:upgrade_template>, [[i1, i1, i1], [i1, i2, i1], [i1, i1, i1]]);

var trim = itemUtils.getItemsByRegexRegistryName("storagedrawersunlimited:trim.*");
<ore:drawerTrim>.addItems(trim);

var drawers = itemUtils.getItemsByRegexRegistryName("storagedrawersunlimited:basicdrawers.*");
<ore:drawerBasic>.addItems(drawers);