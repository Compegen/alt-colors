import crafttweaker.item.IItemStack;
import mods.jei.JEI;

removeRegex("minecraftfuture:.*wall");
recipes.removeByRegex("minecraftfuture:wall.*");

// Wall deduplicate

function removeRegex(exp as string) {
	var allItems = itemUtils.getItemsByRegexRegistryName(exp) as IItemStack[];
	for item in allItems { JEI.removeAndHide(item); }
}